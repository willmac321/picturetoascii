from operator import itemgetter
from PIL import Image, ImageDraw, ImageFont

def countPixelsInASCIIRange(start, end, fontFile, fontSize):
    #http://alexmic.net/letter-pixel-count/
    alphabet = []
    for c in range(start, end):
        alphabet.append(chr(c))
    ''.join(alphabet)

    counts = [
        (letter, count_black_pixels(drawLetter(letter, fontFile, fontSize)))
        for letter in alphabet
    ]
    lst = sorted(counts, key=itemgetter(1), reverse=True)

    return lst

def drawLetter(letter, fontFile, fontSize):
    font = ImageFont.truetype(fontFile, fontSize)
    img = Image.new('RGB', (fontSize, fontSize), 'white')
    draw = ImageDraw.Draw(img)
    draw.text((0,0), letter, font = font, fill='#000000')
    return img


def count_black_pixels(img):
    pixels = list(img.getdata())
    #print(pixels)
    #print(len(list(filter(lambda rgb: sum(rgb) == 0, pixels))))
    return len(list(filter(lambda rgb: sum(rgb) == 0, pixels)))
