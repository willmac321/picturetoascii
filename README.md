#ASCII ART :)
example command:
>  python Art2ASCII.py 'download.jpg' 1 False 32 78 100 True
    
where from the file name on are optional values  
	0.23 is the scale of the image(1 is at image size)  
	False is whether to rotate the image 180 degrees  
	32 is where to start the ASCII code  
	126 is where to end  
	60 is the font size to base the color scale off of  
	True is whether to insert a ' ' as white value, other wise uses min value in supplied range  
   

![img should go here](https://gitlab.com/willmac321/picturetoascii/raw/master/MonaLisa.png)
   

