#!/usr/bin/python3
import numpy as np
import sys
from PixelsInLetters import *
from operator import itemgetter
from PIL import Image, ImageDraw, ImageFont

asciiRange = (32, 126)
fntOut = ImageFont.truetype("consola.ttf", 10)
fontStr = "consola.ttf"

def convertToBlackAndWhite(img, sizeX, sizeY):
    arr = np.array(makeImageSmaller(sizeX, sizeY, img))
    #print ascii char code 32 to 126
    for x  in range(0, len(arr)):
        for y in range(0, len(arr[x])):
            temp = int((int(arr[x][y][0]) + int(arr[x][y][1]) + int(arr[x][y][2])) / 3)
            arr[x][y][0] = temp
            arr[x][y][1] = temp
            arr[x][y][2] = temp
            #arr[x][y][3] = 0
    return arr

def makeImageSmaller(dimX, dimY, img):
    img = img.resize((dimX, dimY), Image.ANTIALIAS)
    return img

def list2dToArray(lst, spaceWhite):
    arry = [(letter[0]) for letter in lst]
    if(spaceWhite == 'True' and arry[-1] != ' '):
        arry.insert(-1,' ')
    return arry

def mapBWtoAscii(img, asciiRankedList, min = asciiRange[0], max = asciiRange[1]):
    lst = asciiRankedList
    stri = " "
    avgW = 0
    avgH = 0
    for i in range(0, len(lst)):
        stri = lst[i]
        w, h = widthOfString(stri)
        if (w > avgW):
            avgW = w
        if(h > avgH):
            avgH = h
    print("fnt: ", avgW, avgH)
    x, y = sizeToMatchFont(img, avgW, avgH)
    arrImage = np.array(img.resize((x, y), Image.ANTIALIAS))
    arrAscii = []
    print("arrImage len: ",len(arrImage[0] ),len(arrImage))
    arrAscii = [[0] * len(arrImage[0]) for i in range(len(arrImage))]
    print("array len: ",len(arrAscii[0]),len(arrAscii))
    for y in range(0, len(arrImage)):
        for x in range(0, len(arrImage[y])):
            arrAscii[y][x] = lst[calcMapToAsciiRange(arrImage[y][x], min, max) - min]
    return arrAscii

def sizeToMatchFont(img, fntW, fntH):
    x, y = img.size
    print("resize: ", (int(round(x / (fntW))), int(round(y / (fntH)))))
    #img.resize((int(round(x / (fntW))), int(round(y / (fntH)))), Image.ANTIALIAS)
    return int(round(x / (fntW))), int(round(y / (fntH)))

def calcMapToAsciiRange(n, min = asciiRange[0], max = asciiRange[1]):
    val = int(round(((max - min - 1) * (n[0])) / (255) + min))
    return val

def widthOfString(str):
    return fntOut.getsize(str)

def widthOfPixelLine(arr, fnt):
    str = ""
    for x in range(0, len(arr[0])):
        str += arr[0][x]
    print("width fo str: ", fnt.getsize(str), len(str))
    return fnt.getsize(str)

def main(imageName, scl = 1, fl = 'True',  min = asciiRange[0], max = asciiRange[1], fntscl = 100, spaceWhite = 'True'):
    img = Image.open(imageName)
    w, h = img.size
    print("img size: ", w, h)
    #img.show()
    lst = list2dToArray(countPixelsInASCIIRange(min, max, fontStr, fntscl), spaceWhite)
    arr = convertToBlackAndWhite(img, round(w * scl) , round(h * scl))

    img = Image.fromarray(arr)
    #img.show()
    if(fl == 'True'):
        img = img.rotate(180)

    asciiArr = mapBWtoAscii(img, lst, min, max)
    stri = ""
    imgStri = ""
    fileOut = open("asciiOut.txt", "w+")


    strW, strH = widthOfPixelLine(asciiArr, fntOut)
    print("height of arr", len(asciiArr))
    imgOut = Image.new('L', (round(strW) , round(strH * len(asciiArr))), 'white')

    #imgOut = Image.open('imgAsciiOut.png')
    dImgOut = ImageDraw.Draw(imgOut)

    j = 0

    for y in range(0, len(asciiArr)):
        for x in range(0, len(asciiArr[y])):
            if(asciiArr[y][x] == '\x9f'):
                asciiArr[y][x] = '\x9b'
            if(asciiArr[y][x] == '\x9d'):
                asciiArr[y][x] = '\x9b'
            if(asciiArr[y][x] == '\x9e'):
                asciiArr[y][x] = '\x9b'
            if(asciiArr[y][x] == '\x2c'):
                asciiArr[y][x] = '\x27'
            if(asciiArr[y][x] == '\x5c'):
                asciiArr[y][x] = '\x2f'
            if(asciiArr[y][x] == '\x60'):
                asciiArr[y][x] = '\x27'
            stri += asciiArr[y][x]#+ " "
            #imgStri += asciiArr[x][y] + " "
        #print(stri)

        dImgOut.text((0, j), stri, font = fntOut, fill = "black")
        j += strH
        fileOut.write(stri+"\r\n")
        stri = ""
    imgOut.save('imgAsciiOut.png')
    imgOut.close()
    fileOut.close()
    print("Image created, check the root file folder for: \n imgAsciiOut.png \n asciiOut.txt")
            # print(arr[x][y][0])
            # print(arr[x][y][1])
            # print(arr[x][y][2])
            # print(arr[x][y][3])


if __name__ == "__main__":
    print("please input an image file path as first arg, 'path.png'")
    print("also optionally supply the x and y dimensions to resize image to")
    print("['image.png'] [Scale\{decimal\}] [rotate=True] [minAscii] [maxAscii] [fontScaling] [addSpaceAsWhite]\n")
    # execute only if run as a script
    if(len(sys.argv[1:]) > 0 and len(sys.argv[1:]) < 3):
        main(sys.argv[1], float(sys.argv[2]))
    elif(len(sys.argv[1:]) > 0 and len(sys.argv[1:]) < 4):
        main(sys.argv[1], float(sys.argv[2]), sys.argv[3])
    elif(len(sys.argv[1:]) > 0 and len(sys.argv[1:]) < 8):
        main(sys.argv[1], float(sys.argv[2]), sys.argv[3], int(sys.argv[4]), int(sys.argv[5]), int(sys.argv[6]), sys.argv[7])
